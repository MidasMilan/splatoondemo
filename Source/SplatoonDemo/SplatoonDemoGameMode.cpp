// Copyright Epic Games, Inc. All Rights Reserved.

#include "SplatoonDemoGameMode.h"
#include "SplatoonDemoCharacter.h"
#include "UObject/ConstructorHelpers.h"

ASplatoonDemoGameMode::ASplatoonDemoGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
