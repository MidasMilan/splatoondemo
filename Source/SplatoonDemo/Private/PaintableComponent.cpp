// Fill out your copyright notice in the Description page of Project Settings.


#include "PaintableComponent.h" 	
#include "GameFramework/Character.h" 
#include <string>
#include "DrawDebugHelpers.h"
#include <Runtime\Engine\Classes\Kismet\KismetRenderingLibrary.h>

// Sets default values for this component's properties
UPaintableComponent::UPaintableComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}

void UPaintableComponent::PaintThisActor(UObject* WorldContextObject, const FVector& HitLocation, const float PaintRadius)
{
	UE_LOG(LogTemp, Warning, TEXT("Starting Paint this actor in c++"));
	if (!m_brushInstance)
	{
		UE_LOG(LogTemp, Warning, TEXT("material instance not valid..."));
	}

	if (!m_renderTargetInstance)
	{
		UE_LOG(LogTemp, Warning, TEXT("material rendertarget not valid..."));
	}

	m_brushInstance->SetVectorParameterValue("uvTransform", HitLocation);
	UE_LOG(LogTemp, Warning, TEXT("Your string: %s"), *HitLocation.ToString());
	UKismetRenderingLibrary::DrawMaterialToRenderTarget(WorldContextObject, m_renderTargetInstance, m_brushInstance);
}

// Called when the game starts
void UPaintableComponent::BeginPlay()
{
	Super::BeginPlay();

	UMeshComponent* actorComp = GetOwner()->FindComponentByClass<UMeshComponent>();
	if (!actorComp) return;
	FSoftObjectPath MaterialPath("Material'/Game/_Core/Materials/MAT_Paintable'");
	UObject* MaterialAsset = MaterialPath.TryLoad();
	UMaterial* MaterialPointer = (UMaterial*)MaterialAsset;
	if (!MaterialPointer)
	{
		UE_LOG(LogTemp, Warning, TEXT("Cant find the Material..."));
		return;
	}

	UMaterialInstanceDynamic* objectMatInstance = actorComp->CreateAndSetMaterialInstanceDynamicFromMaterial(0, MaterialPointer);

	FStringAssetReference BrushSamplePath("Material'/Game/_Core/Materials/Mat_PaintBrush'");
	UObject* BrushAssetPath = BrushSamplePath.TryLoad();
	UMaterial* m_brushMaterial = (UMaterial*)BrushAssetPath;
	m_brushInstance = actorComp->CreateDynamicMaterialInstance(1, m_brushMaterial);

	m_renderTargetInstance = UKismetRenderingLibrary::CreateRenderTarget2D(GetWorld(), 2048, 2048);
	objectMatInstance->SetTextureParameterValue("PaintRT", m_renderTargetInstance);

	UE_LOG(LogTemp, Warning, TEXT("Don"));
}


// Called every frame
void UPaintableComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

